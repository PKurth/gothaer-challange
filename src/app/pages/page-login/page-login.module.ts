import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {PageLoginRoutingModule} from './page-login-routing.module';
import {PageLoginComponent} from './components/page-login/page-login.component';
import {FormsModule} from '@angular/forms';


@NgModule({
    declarations: [PageLoginComponent],
    imports: [
        CommonModule,
        /*
         * using templatedriven Forms because i think ReactiveForms are bad
         * @see https://blog.angularindepth.com/a-proposal-to-improve-angulars-reactiveformsmodule-323594fe2d74
         */
        FormsModule,
        PageLoginRoutingModule
    ]
})
export class PageLoginModule {
}
