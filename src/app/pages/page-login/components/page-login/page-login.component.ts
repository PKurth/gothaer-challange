import {Component} from '@angular/core';
import {AuthService} from '../../../../core/auth/services/auth.service';
import {NgForm} from '@angular/forms';
import {first} from 'rxjs/operators';
import {Router} from '@angular/router';

@Component({
    selector: 'app-page-login',
    templateUrl: './page-login.component.html',
    styleUrls: ['./page-login.component.scss']
})
export class PageLoginComponent {
    public error: string;

    public constructor(private authService: AuthService,
                       private router: Router) {
    }

    public submit(loginForm: NgForm) {
        if (loginForm.valid) {
            this.authService.login(loginForm.value.username, loginForm.value.password).pipe(first()).subscribe(
                () => this.router.navigate(['']),
                error => this.error = error
            );
        }
    }
}
