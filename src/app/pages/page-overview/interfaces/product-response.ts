export interface ProductResponse {
    name: string;
    availability: string;
    prices?: Price[];
    info?: object;
    type: string;
}
