import {Membership} from './membership';

export interface Product {
    name: string;
    availability: Membership;
    prices?: Price[];
    info?: object;
    type: string;
}
